function tdtBlockData=tdt_extract_preprocess(blockPath,paramsEP)
%asks for a block if blockpath is empty

%USER DEFINED:
%=============
% tdtServer='Local';
% userName='ntmh2';
% tdtTank='LFP_tank';
% tdtBlock='LFP_tank-25';
butterOrder=5;

%GET PARAMS:
%===========
%read parameters
tdtServer   = paramsEP.tdtServer; %'Local'
userName    = paramsEP.userName;
% maxLFPch    = paramsEP.maxLFPch; %24
% getTankLFP  = paramsEP.getTankLFP; %false
% maxUnitCh   = paramsEP.maxUnitCh; %24
% maxUnitCode = paramsEP.maxUnitCode; %2
% getSnips    = paramsEP.getSnips; %false
% readFrame   = paramsEP.readFrame; %90 seconds
if isfield(paramsEP,'getLFP')
    getLFP=paramsEP.getLFP;
else
    getLFP=true;
end
if getLFP
    maxLFPch = paramsEP.maxLFPch; %24
    if isfield(paramsEP,'getTankLFP')
        getTankLFP = paramsEP.getTankLFP; %false
    else
        getTankLFP = false;
    end    
end

if isfield(paramsEP,'getUnit')
    getUnit=paramsEP.getUnit;
else
    getUnit=true;
end
if getUnit
    maxUnitCh   = paramsEP.maxUnitCh; %24
    maxUnitCode = paramsEP.maxUnitCode; %2
    getSnips    = paramsEP.getSnips; %false
    readFrame   = paramsEP.readFrame; %90 seconds    
end

if isfield(paramsEP,'getTask')
    getTask=paramsEP.getTask;
else
    getTask=false;
    targSep=[];
end
if getTask
    targSep=paramsEP.targSep;
    if isfield(paramsEP,'targOffset')
        targOffset=paramsEP.targOffset;
    else
        targOffset=0;
    end
end

if isfield(paramsEP,'getRate')
    getRate=paramsEP.getRate;
else
    getRate=false;
end
if isfield(paramsEP,'getLfLFP')
    getLfLFP=paramsEP.getLfLFP;
else
    getLfLFP=false;
end
if getLfLFP
    maxLfLFPch = paramsEP.maxLfLFPch; %24
end

if isfield(paramsEP,'getJPCs')
    getJPCs=paramsEP.getJPCs;
else
    getJPCs=false;
end
if isfield(paramsEP,'getEMGs')
    getEMGs=paramsEP.getEMGs;
else
    getEMGs=false;
end
if getEMGs
    theseEMGch=paramsEP.theseEMGch;
    downsamEMGfac=paramsEP.downsamEMGfac;    
end

%data cleaning parameters
cutFirst    = paramsEP.cutFirst; %samples or 'auto'
cutLast     = paramsEP.cutLast; %samples
detrendType = paramsEP.detrendType; %'none','constant', etc.

%preprocess parameters
if isfield(paramsEP,'toPreprocess')
    toPreprocess=paramsEP.toPreprocess;
else
    toPreprocess=true;
end
lowSRtarget = paramsEP.lowSRtarget; %Hz target sampling rate (low)
filtHP      = paramsEP.filtHP; %Hz
filtLP      = paramsEP.filtLP; %Hz


%GET THE BLOCK LOCATION:
%======================
if isempty(blockPath)
    [fileName blockPath]=uigetfile('*.*','Select TEV file from block');
else
    cd(blockPath)
    D=dir('*.tev');
    if length(D)==1
        fileName=D.name;
    else
        error('more than one tev file in this folder')
    end
end

if ~ischar(fileName) %the user cancelled the interface
    fprintf('\rYou didn`t specify a file!\r\r');
    tdtBlockData=[];
    return
end

if strcmp(blockPath(end),'\')
    blockPath(end)=[];
end
puncPoints=regexp(blockPath,'\');
tankPath=blockPath(1:puncPoints(end)-1);
tdtBlock=blockPath(puncPoints(end)+1:end)
cd(tankPath)

%CONNECT TO TDT BLOCK:
%=====================
hActX=figure(9999);
set(hActX,'Position',[20 60 100 100],'HandleVisibility','off');
TTX=actxcontrol('TTank.X','parent',hActX);

if ~TTX.ConnectServer(tdtServer,userName);
    error('cant connect to server: %s with username: %s',tdtServer,userName);
end
if ~TTX.OpenTank(tankPath,'R');
    error('cant open tank: %s',tdtTank)
end
if ~TTX.SelectBlock(tdtBlock);
    error('error selecting block: %s',tdtBlock)
end

%get the recording date/time from the TDT file:
start    = TTX.CurBlockStartTime;
recStart = TTX.FancyTime(start,'D-O-Y H:M:S');
recStart = datenum(recStart);

%EXTRACT THE WAVEFORM CHANNELS:
%==============================

%GET SOME INFO:
notes=TTX.CurBlockNotes;

%get the base SR
junk=TTX.ReadEventsV(1,'eNeu',1,0,0,0,'NODATA');
srBase=100e6/2^12;%TTX.ParseEvInfoV(0,0,9)

if getLFP
    %find the number of points per eventBlock for the LFP data:
    iData=regexp(notes,'NAME=HeadName;TYPE=T;VALUE=Wave;')';
    iPoints=regexp(notes,'NAME=NumPoints;TYPE=L;VALUE=')';
    iLine=iPoints(find(iPoints>iData,1));
    noteLine=notes(iLine:iLine+50);
    iVal=regexp(noteLine,'VALUE=');
    iCR=regexp(noteLine,'\r');
    nPointsLFP=str2double(noteLine(iVal+6:iCR-2)); %usually 16
    
    %read the events in channel 1 once to get the number of eventBlocks and
    %sRate
    tellme(0,'Checking LFPs info...')
    nEvBlocksLFP=TTX.ReadEventsV(1e7,'Wave',1,0,0,0,'NODATA');
    srLFPt=TTX.ParseEvInfoV(0,0,9)
end

if getUnit&&~getLFP
    %get the srLFPt because cutFurst is defined in terms of this value in
    %samples at the tank lfp Fs
    junk=TTX.ReadEventsV(1,'Wave',1,0,0,0,'NODATA');
    srLFPt=TTX.ParseEvInfoV(0,0,9)
end

%do the same for task and rate and jPCs
%for these I know the nPoints. may need to modify this in the future, but
%for now I'm just gonna hard code them here
nPointsTask=32;
nChanTask=8;
nPointsRate=32;
nChanRate=8;
nPointsLfLFP=16;
nChanLfLFP=32;
nPointsJPCs=16;
nChanJPCs=16;
nPointsEMGs=1024;

if getTask
    tellme(0,'\rChecking Task info...')
    nEvBlocksTask=TTX.ReadEventsV(1e7,'Task',1,0,0,0,'NODATA');
    srTask=TTX.ParseEvInfoV(0,0,9)
end
if getRate %online firing rates - usually ignore
    tellme(0,'\rChecking Rate info...')
    nEvBlocksRate=TTX.ReadEventsV(1e7,'Rate',1,0,0,0,'NODATA');
    srRate=TTX.ParseEvInfoV(0,0,9)
end
if getEMGs
    tellme(0,'\rChecking EMGr info...')
    nEvBlocksEMGs=TTX.ReadEventsV(1e7,'EMGr',1,0,0,0,'NODATA');
    srEMGs=TTX.ParseEvInfoV(0,0,9)
end
if getLfLFP
    tellme(0,'\rChecking lf-LFPs info...')
    nEvBlocksLfLFP=TTX.ReadEventsV(1e7,'LLFP',1,0,0,0,'NODATA');
    srLfLFP=TTX.ParseEvInfoV(0,0,9)
end  
if getJPCs
    tellme(0,'\rChecking jPCs info...')
    nEvBlocksJPCs=TTX.ReadEventsV(1e7,'jPCs',1,0,0,0,'NODATA');
    srJPCs=TTX.ParseEvInfoV(0,0,9)
end        

%LOAD THE LFP DATA PER CHANNEL:
if getLFP
    tellme(1,'\rExtracting LFPs...')
    multiWaitbar('Extracting LFPs...',0);
    %prepad the LFP vector:
    lfpsT=nan(nEvBlocksLFP*nPointsLFP,maxLFPch);
    %lfpsT=lfps from the 'Tank'
    for ch=1:maxLFPch
        %cache all the events for that channel:
        evs=TTX.ReadEventsV(nEvBlocksLFP,'Wave',ch,0,0,0,'ALL');
        %get the datapoints:
        y=TTX.ParseEvV(0,evs);
        %reshape into a vector:
        y=reshape(y,[],1);
        %put into the matrix as a double:
        lfpsT(:,ch)=double(y);
        multiWaitbar('Extracting LFPs...',ch/maxLFPch);
    end
    clear y
end

if getTask
    tellme(1,'\rExtracting Task...')
    multiWaitbar('Extracting TaskData...',0);
    %prepad the task vector:
    taskDataT=nan(nEvBlocksTask*nPointsTask,nChanTask);
    
    for ch=1:nChanTask
        %cache all the events for that channel:
        evs=TTX.ReadEventsV(nEvBlocksTask,'Task',ch,0,0,0,'ALL');
        %get the datapoints:
        y=TTX.ParseEvV(0,evs);
        %reshape into a vector:
        y=reshape(y,[],1);
        %put into the matrix as a double:
        taskDataT(:,ch)=double(y);
        multiWaitbar('Extracting TaskData...',ch/nChanTask);
    end
    clear y
    
    % POST-PROCESS THE TASK DATA TO GET TRIAL EVENTS
    % ==============================================
    
    t_pos=round(taskDataT(:,1:2)*100);
    c_pos=round(taskDataT(:,3:4)*100);
    
    tolerance=10;
    
    t_pos(find(abs(t_pos)<tolerance))=0;
    tdist=sqrt(t_pos(:,1).^2+t_pos(:,2).^2);
    ev4=find((tdist(2:end)==0)&(tdist(1:end-1)>0));
    ev2=find((tdist(2:end)>0)&(tdist(1:end-1)==0));
    while max(ev2)>max(ev4)
        ev2(end)=[];
    end
    while min(ev4)<min(ev2)
        ev4(1)=[];
    end
    
    pt=t_pos(ev4-2,:);
    tang=round(180*atan2(pt(:,1),pt(:,2))/pi);
    ti=1;
    targ=[];
    while ~isempty(tang)
        targ(ti).ang=min(tang);
        ii=find(abs(tang-targ(ti).ang)<tolerance);
        targ(ti).t_pos=pt(max(ii),:);
        targ(ti).n=0;
        targ(ti).tlist=[];
        ang(ti)=targ(ti).ang;
        tang(ii)=[];pt(ii,:)=[];
        ti=ti+1;
    end
    tc=1;
    numtarg=ti-1;
    
    numtrials=length(ev4)-1;
    trial=[];
    for ii=1:numtrials
        trial(ii).ev(4)=ev4(ii)/srTask;
        trial(ii).ev(2)=max(ev2(find(ev2<ev4(ii))))/srTask;
        trial(ii).ev_description={'' 'startTrial' '' 'endSuccessfulHold'};
        trial(ii).ang=round(180*atan2(t_pos(ev4(ii)-2,1),t_pos(ev4(ii)-2,2))/pi);
        trial(ii).angrd=round(round(180*atan2(t_pos(ev4(ii)-2,1),t_pos(ev4(ii)-2,2))/pi)./45)*45; %round to nearest 45deg
        trial(ii).targ=find(abs(ang-trial(ii).ang)<tolerance);        
    end
    
    fprintf('%i trials loaded (%i targets)\r\r',length(trial),max([trial.targ]))
        
    
end

if getRate
    tellme(1,'\rExtracting Rate...')
    multiWaitbar('Extracting RateData...',0);
    %prepad the rate vector:
    rateDataT=nan(nEvBlocksRate*nPointsRate,nChanRate);
    
    for ch=1:nChanRate
        %cache all the events for that channel:
        evs=TTX.ReadEventsV(nEvBlocksRate,'Rate',ch,0,0,0,'ALL');
        %get the datapoints:
        y=TTX.ParseEvV(0,evs);
        %reshape into a vector:
        y=reshape(y,[],1);
        %put into the matrix as a double:
        rateDataT(:,ch)=double(y);
        multiWaitbar('Extracting RateData...',ch/nChanRate);
    end
    clear y
end

if getEMGs
    tellme(1,'\rExtracting EMGs...')
    multiWaitbar('Extracting EMGsData...',0);
    %prepad the rate vector:
    emgsData=nan(ceil((nEvBlocksEMGs*nPointsEMGs)/downsamEMGfac),length(theseEMGch));
    
    for ii=1:length(theseEMGch)
        ch=theseEMGch(ii);
        %cache all the events for that channel:
        evs=TTX.ReadEventsV(nEvBlocksEMGs,'EMGr',ch,0,0,0,'ALL');
        %get the datapoints:
        y=TTX.ParseEvV(0,evs);
        %reshape into a vector:
        y=reshape(y,[],1);
        y=double(y);
        
        %downsample if requested
        if downsamEMGfac>1
            y=resample(y,1,downsamEMGfac);
        end
        
        %put into the matrix as a double:
        emgsData(:,ii)=y;
        multiWaitbar('Extracting EMGsData...',ii/length(theseEMGch));
    end
    clear y
end

if getLfLFP
    tellme(1,'\rExtracting lf-LFPs...')
    multiWaitbar('Extracting lfLFPdata...',0);
    %prepad the lf-LFP matrix:
    lfLFPdataT=nan(nEvBlocksLfLFP*nPointsLfLFP,maxLfLFPch);
    
    for ch=1:maxLfLFPch
        %cache all the events for that channel:
        evs=TTX.ReadEventsV(nEvBlocksLfLFP,'LLFP',ch,0,0,0,'ALL');
        %get the datapoints:
        y=TTX.ParseEvV(0,evs);
        %reshape into a vector:
        y=reshape(y,[],1);
        %put into the matrix as a double:
        lfLFPdataT(:,ch)=double(y);
        multiWaitbar('Extracting lfLFPdata...',ch/maxLfLFPch);
    end
    clear y    
end

if getJPCs
    tellme(1,'\rExtracting jPCs...')
    multiWaitbar('Extracting jPCsData...',0);
    %prepad the jpc vector:
    jpcsDataT=nan(nEvBlocksJPCs*nPointsJPCs,nChanJPCs);
    
    for ch=1:nChanJPCs
        %cache all the events for that channel:
        evs=TTX.ReadEventsV(nEvBlocksJPCs,'jPCs',ch,0,0,0,'ALL');
        %get the datapoints:
        y=TTX.ParseEvV(0,evs);
        %reshape into a vector:
        y=reshape(y,[],1);
        %put into the matrix as a double:
        jpcsDataT(:,ch)=double(y);
        multiWaitbar('Extracting jPCsData...',ch/nChanJPCs);
    end
    clear y
end

%GET THE SPIKE TIMES:
%====================
if getUnit
    tellme(1,'\rExtracting Spikes...')
    multiWaitbar('Extracting spike data...',0);
    %GET SOME INFO:
    lenData=TTX.CurBlockStopTime-TTX.CurBlockStartTime;
    nChunk=ceil(lenData/readFrame);
    %set up the repository
    units(maxUnitCh,maxUnitCode)=struct('spTimes',[],'snips',[]);
%     gotSR=false;
    %get the data:
    for c=1:nChunk

        T1=(c-1)*readFrame; %read from >=
        T2=min(c*readFrame,lenData); %read to <

        for ch=1:maxUnitCh
            for sc=1:maxUnitCode
                %get the spike events:
                nSpikes=TTX.ReadEventsV(1e6,'eNeu',ch,sc,T1,T2,'ALL');
                if nSpikes>0
                    %get the spike times:
                    units(ch,sc).spTimes=[units(ch,sc).spTimes,TTX.ParseEvInfoV(0,nSpikes,6)];
                    if getSnips
                        %get the spike waveforms:
                        units(ch,sc).snips=[units(ch,sc).snips,TTX.ParseEvV(0,nSpikes)];
                    end                
%                     if ~gotSR
%                         %get the base sampling rate
%                         srBase=TTX.ParseEvInfoV(0,0,9);
%                         gotSR=true;
%                     end
                end
            end
        end
        multiWaitbar('Extracting spike data...',c/nChunk);
    end
end

%HOUSE KEEPING:
multiWaitbar('Extracting LFPs...','Close');
multiWaitbar('Extracting TaskData...','Close');
multiWaitbar('Extracting RateData...','Close');
multiWaitbar('Extracting jPCsData...','Close');
multiWaitbar('Extracting spike data...','Close');
multiWaitbar('Extracting EMGsData...','Close');
multiWaitbar('Extracting lfLFPdata...','Close');

TTX.CloseTank
TTX.ReleaseServer
close(hActX)

%CLEAN THE DATA.
%===============

%CUT THE BEGINNING (BEFORE THE FILTERS) AND ENDS (ZEROS) OF THE REC.
%if 'auto' then calculate cutFirst
if ischar(cutFirst)
    if strcmp(cutFirst(1:4),'auto')
        iCh=str2double(cutFirst(6:8));
        thresh=str2double(cutFirst(10:end));
        lch=lfpsT(:,iCh);
        dL=diff(lch);
        s=find(dL>thresh,1);
%         s=tdt_windup_finder(lfps,thresh,srLFP);
        if isempty(s)
            cutFirst=0;
        else
            cutFirst=s+20;
        end
    else
        error('cutFirst must be integer samples or string e.g "auto:02:3e-4"')
    end
else
    cutFirst=cutFirst; %samples
end

cutFirstT=cutFirst/srLFPt; %convert samples to secs

if getLFP
    % remove a section of data off the end of all the LFPs as TDT leaves a
    % string of zeros:
    lfpsT(end-cutLast:end,:)=[];
    %check that the cutLast has been long enough
    if sum(lfpsT(end,:)==0)~=0
        error('still some zeros at the end of the LFP data')
    end
    
    %remove a section of the beginning of the LFPs before the filters kick in:
    tellme(0,'cutFirst %i samples',cutFirst)
    lfpsT(1:cutFirst,:)=[];
end

if getUnit
    %remove cutFirst (seconds) from the spike times as they are in absolute time relative to start of recording:
    %do this here in a for loop for clarity, rather than during the import.

    tellme(0,'\rcutFirst %.3f seconds\r',cutFirstT)
    for chan=1:size(units,1)
        for code=1:size(units,2)
            units(chan,code).spTimes=units(chan,code).spTimes-cutFirstT;
            units(chan,code).spTimes=units(chan,code).spTimes(units(chan,code).spTimes>0);
            if getSnips
                units(chan,code).snips=units(chan,code).snips(:,units(chan,code).spTimes>0);
            end
        end
    end
end

if getLFP
    % detrend the LFP (if requested) and output he unfiltered LFP
    % at this point if requested
    switch detrendType
        case 'none'
            tellme(1,'\rNOT detrending LFPs')
        case 'constant'
            tellme(1,'\rDetrending LFPs: %s','constant')
            lfpsT=detrend(lfpsT,'constant');
    end
end

%2014-05-15tmh : adding in cutFirst for Task, as it's at the same SR as the
%LFPs.
if getTask

    if (srTask~=srLFPt)&&(cutFirst~=0)
        error('not yet implemented for blocks where srLFPt~=srTask')
    end
    
    taskDataT(end-cutLast:end,:)=[];
    taskDataT(1:cutFirst,:)=[]; %from the data
    
    %adjust trial times (this doesn't do cutlast)
    negtrials=[];
    for ii=1:length(trial)
        trial(ii).ev(2)=trial(ii).ev(2)-cutFirstT;
        trial(ii).ev(4)=trial(ii).ev(4)-cutFirstT;
        if trial(ii).ev(2)<=0
            negtrials=[negtrials,ii];
        end
    end
    
%     negtrials
    if ~isempty(negtrials)
        tellme(1,'\rRemoving %i trials because of cutFirst',length(negtrials));
        trial(negtrials)=[];
    end
    
end

%OUTPUT VARIABLES 1
tdtBlockData.tankPath=tankPath;
tdtBlockData.tdtBlock=tdtBlock;
tdtBlockData.blockDateStr=datestr(recStart);
tdtBlockData.recStart=recStart;
tdtBlockData.srBase=srBase;
if getLFP
    tdtBlockData.lfps.srLFPt=srLFPt;
end

if getLFP&&getTankLFP
    tellme(2,'\rOutputting tank LFPs')
    tdtBlockData.lfps.lfpsT=lfpsT;    
else
    tellme(2,'\rNot outputting tank LFPs')
    tdtBlockData.lfps.lfpsT=[];
end

if getTask
    tdtBlockData.task.taskDataT=taskDataT;
    tdtBlockData.task.srTask=srTask;
    tdtBlockData.trial=trial;
    tdtBlockData.targ=targ;
%     tdtBlockData.task.trialEv=trialEv;
%     tdtBlockData.task.trialEvS=trialEvS;
end
if getRate
    tdtBlockData.rate.rateDataT=rateDataT;
    tdtBlockData.rate.srRate=srRate;
end
if getLfLFP
    tdtBlockData.lfLFP.lfLFPdataT=lfLFPdataT;
    tdtBlockData.lfLFP.srLfLFP=srLfLFP;
end
if getJPCs
    tdtBlockData.jpcs.jpcsDataT=jpcsDataT;
    tdtBlockData.jpcs.srJPCs=srJPCs;
end
if getEMGs
    tdtBlockData.emgs.emgsData=emgsData;
    tdtBlockData.emgs.srEMGs=srEMGs;
    if downsamEMGfac>1
        tdtBlockData.emgs.downsampled='DOWNSAMPLED';
        tdtBlockData.emgs.downsamEMGfac=downsamEMGfac;
        tdtBlockData.emgs.srEMGs_DS=srEMGs/downsamEMGfac;
    end
end
if getEMGs||getRate||getJPCs
    fprintf('\r')
    warning('Rate and JPCs and EMGs are not cut like the lfps/srate. Use them only with cutfirst==0')
end


%PRE-PROCESS (FILTER/DOWNSAMPLE LFPS (HP+/-LP), CALCULATE FIRING RATES)
%======================================================================

if toPreprocess
    if getLFP
        % LFPS:

        %high-pass filter the lfps to get rid of DC offset
        %doesn't make sense to do this with the detrend as well
        if isempty(filtHP)
            tellme(1,'\rNOT high-pass filtering LFPs')
        else
            tellme(1,'\rHigh-pass filtering LFPs: %.4fHz (5-pole butterworth)',filtHP)
            [hpB,hpA]=butter(5,(filtHP/(srLFPt/2)),'high');
            lfpsT=filtfilt(hpB,hpA,lfpsT);
        end

        %calculate resamping factor to get down to target low Fs
        rsFac=round(srLFPt/lowSRtarget);
        tellme(2,'\rOriginal LFP sRate = %5.2f Hz',srLFPt)
        srLFPlow=srLFPt/rsFac;
        tellme(2,'\rNew low sRate = %5.2f Hz',srLFPlow)

        if rsFac<=1
            %if the existing sampling rate is lower or equal to the target then
            %don't resample!!
            tellme(1,'\rNot downsampling LFPs... SRtank <= SRtarget!')
            lfpsD=lfpsT;
        else
            %downsample the lfps (lfpsD for downsampled)
            lfpsD=nan(ceil(size(lfpsT,1)/rsFac),size(lfpsT,2));
            tellme(1,'\rDownsampling LFPs (using resample.m)      ')
            for ii=1:size(lfpsT,2)
                tellme(2,'\b\b\b\b\b%.2i/%.2i',ii,size(lfpsT,2))
                %     lfps_D(:,ii)=decimate(lfps(:,ii),rsFac);
                %15/11/12: changed to resample as decimate seems to shift data by one sample.
                lfpsD(:,ii)=resample(lfpsT(:,ii),1,rsFac);
                multiWaitbar('Downsampling LFPs...',ii/size(lfpsT,2)) ;
                %     multiWaitbar('Decimating LFPs...',ii/size(lfps,2)) ;
            end
            multiWaitbar('Downsampling LFPs...','Close') ;
        end
        clear lfpsT
    end
    
    if getUnit
        % SPIKES

        %bin the spike times (firing rates):
        timeVec=(0:size(lfpsD,1)-1)/srLFPlow;
        binWidth=(timeVec(2)-timeVec(1));
        tellme(2,'\rBin width = %5.3f ms',binWidth*1000)
        tellme(2,'\rData length = %.1f sec (%.1f min)',max(timeVec),max(timeVec)/60)

        for chan=1:size(units,1)
            for code=1:size(units,2)
                if isempty(units(chan,code).spTimes)
                    units(chan,code).spRate=[];
                    %also make the spTimes a single NaN
                    %             units(chan,code).spTimes=[];
                else
                    units(chan,code).spRate=hist(units(chan,code).spTimes,timeVec);
                    units(chan,code).binWidth=binWidth;
                    %remove the last sample
                    %because the spike rate will be inflated in the last bin if the lfp is
                    %shorter than the highest few spike times:
                    %(which it will be because cutLast removes the end of the LFP)
                    %(but it often is anyway because the TDT system seems to stop
                    %recording spike times later)
                    units(chan,code).spRate(end)=[];
                end
            end
        end
    end
    
    %also remove the final sample from the decimated LFP so that it's the same
    %length as the spRates.
    if getLFP
        lfpsD(end,:)=[];
    end
    %for clarity or plotting during debug, also remove that sample from the LF timeVec
    if getUnit
        timeVec(end)=[]; %#ok<NASGU>
    end
    
    %FINALLY, LOW-PASS FILTER BOTH THE FIRING RATES AND THE LFPs.
    if getLFP
        if isempty(filtLP)
            tellme(1,'\rNOT low-pass filtering LFPs')
            lfpsF=lfpsD;
        else
    %         tellme(1,'\rLow-pass filtering LFPs and firing rates: %.1fHz (%ith order butterworth)',filtLP,butterOrder)
            tellme(1,'\rLow-pass filtering LFPs: %.1fHz (%ith order butterworth)',filtLP,butterOrder)
            [lpB,lpA]=butter(butterOrder,(filtLP/(srLFPlow/2)),'low');
            lfpsF=filtfilt(lpB,lpA,lfpsD);
            % lfpsF=lfpsD;
            clear lfpsD
        end
    end
    
    if getUnit
        if isempty(filtLP)
            tellme(1,'\rNOT low-pass filtering firing rates')
        else
            tellme(1,'\rLow-pass filtering firing rates: %.1fHz (%ith order butterworth)',filtLP,butterOrder)
            for chan=1:size(units,1)
                for code=1:size(units,2)
                    if isempty(units(chan,code).spRate)
                        units(chan,code).spRateF=[];
                    else
                        units(chan,code).spRateF=filtfilt(lpB,lpA,units(chan,code).spRate);
                    end
                    units(chan,code).tdtBlock=tdtBlock;
                end
            end
        end
    end
    
    if getLFP
        %OUTPUT VARIABLES 2
        tdtBlockData.lfps.lfpsF=lfpsF;
        tdtBlockData.lfps.srLFPlow=srLFPlow;
    end

else
    if getLFP
        %     tdtBlockData.lfps.lfpsF=[];
        tdtBlockData.lfps.srLFPlow='not preprocessed';
    end
end

if getLFP
    tdtBlockData.lfps.chan=1:maxLFPch;
end
if getUnit
    tdtBlockData.units=units;
end
tdtBlockData.paramsEP=paramsEP;   

%HOUSE KEEPING
%=============
tellme(1,'\rDONE!\r\r')

end %EOF