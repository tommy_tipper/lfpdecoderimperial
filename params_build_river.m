function params_lfpdBu=params_build_river

params_lfpdBu.fitProp=      0.75;
params_lfpdBu.width=        200;
params_lfpdBu.nComp=        6;
params_lfpdBu.maxUnitCh=    24;
params_lfpdBu.maxUnitCode=  2;
params_lfpdBu.minSpRate=    3;
params_lfpdBu.maxLFPCh =    24;
% params_lfpdBu.nUnits=2;
params_lfpdBu.noiseF=       100;
% params_lfpdBu.badLFPsSTDthresh=[1e-10 1];
params_lfpdBu.badLFPCh=[3 5];
% params_lfpdBu.badLFPCh=[3 5 27 38 41 45 54 55 56];
params_lfpdBu.badUnitCh=  [];
params_lfpdBu.lfpColorBase=[repmat('r',1,12),repmat('b',1,12),repmat('g',1,32)];
                          
% %smoothing
% params_lfpdBu.smoothType= 'lpFilter'; %'expDecay';%
% params_lfpdBu.lpForSmoothing=1; %hz
% params_lfpdBu.alphaForExpDecay=0.125;

end