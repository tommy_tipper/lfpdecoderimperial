function params_lfpdBu=params_build_silver

params_lfpdBu.fitProp=      0.75;
params_lfpdBu.width=        200;
params_lfpdBu.nComp=        6;
params_lfpdBu.maxUnitCh=    68;
params_lfpdBu.maxUnitCode=  2;
params_lfpdBu.minSpRate=    3;
params_lfpdBu.maxLFPCh =    68;
% params_lfpdBu.nUnits=2;
params_lfpdBu.noiseF=       100;
% params_lfpdBu.badLFPsSTDthresh=[1e-10 1];
params_lfpdBu.badLFPCh=     [25 26 27 28 29 31 33 35 37 38 40 41 42 55 58];
params_lfpdBu.badUnitCh=    [25:56];
params_lfpdBu.lfpColorBase=[repmat('r',1,12),repmat('b',1,12),repmat('r',1,32),repmat('r',1,32),repmat('b',1,12)];
                          
% %smoothing
% params_lfpdBu.smoothType= 'lpFilter'; %'expDecay';%
% params_lfpdBu.lpForSmoothing=1; %hz
% params_lfpdBu.alphaForExpDecay=0.125;

end