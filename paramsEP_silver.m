function paramsEP_lfpd=paramsEP_silver

%read parameters
paramsEP_lfpd.tdtServer='Local';
paramsEP_lfpd.userName='a9921268';
paramsEP_lfpd.maxLFPch=68;
paramsEP_lfpd.getTankLFP=false;
paramsEP_lfpd.maxUnitCh=68;
paramsEP_lfpd.maxUnitCode=2;
paramsEP_lfpd.getSnips=false;
paramsEP_lfpd.readFrame=90; % seconds

%preprocess parameters
paramsEP_lfpd.cutFirst=100; %'auto:009:2e-4'; %SAMPLES to remove at the start before the filters kick in
%                                       %OR
%                                       %string = 'auto:iCh:[thresh]'
paramsEP_lfpd.cutLast=125; %samples
paramsEP_lfpd.lowSRtarget=50; %Hz target sampling rate (low); zero for no resample.
paramsEP_lfpd.detrendType='constant'; %'none','constant', etc.
paramsEP_lfpd.filtHP=[]; %Hz
paramsEP_lfpd.filtLP=5; %Hz

paramsEP_lfpd.getTask=true;
paramsEP_lfpd.targSep=0.25;

end