function tellme(verbosity,varargin)
%FUNCTION TELLME(VERBOSITY,VARARGIN)
%
%Wrapper function
%If the verbosity of a statement exceeds the requested (global) verbosity
%level, then passes varargin to fprintf, which then behaves as normal.
%T. Hall 2012

global VERBOSITY_LEVEL

if verbosity<=VERBOSITY_LEVEL
    fprintf(varargin{:})
end

end