function [decoderLFP,testResB,feature,H,extras]=lfpDecoder_multiU_build(lfp,unit,lfpChList,unitList,unitsOfInt,params_lfpdBu,params_lfpdMC)
% Function for building a firing rate estimation model from lf-LFPs.
%

% 2014-04-30tmh : added 'legacy' to ismember behavior.

global VERBOSITY_LEVEL  %between 0 and 2 to specify how much info tellme.m gives out.
                        %must be defined as a global variable in main workspace

%% LOAD PARAMETERS
%==========================================================================
tellme(2,'\rParameters for LFP decoder:\r')
if VERBOSITY_LEVEL>1
    disp(params_lfpdBu)
end

%GET BUILD PARAMS

fitProp     = params_lfpdBu.fitProp; %0.75
width       = params_lfpdBu.width ; % 200;
nComp       = params_lfpdBu.nComp ; %6; %=n components
maxUnitCh   = params_lfpdBu.maxUnitCh ; %max ch to take units
maxUnitCode = params_lfpdBu.maxUnitCode ; %max code to take units
minSpRate   = params_lfpdBu.minSpRate ;%5hz
badUnitCh   = params_lfpdBu.badUnitCh;
maxLFPCh    = params_lfpdBu.maxLFPCh  ; %max ch of LFP to use
badAlways   = params_lfpdBu.badLFPCh ; %[12];
noiseF      = params_lfpdBu.noiseF; %100
lfpColors   = params_lfpdBu.lfpColorBase;

%LOGICAL TO COPE WITH CALLS WITHOUT Montecarlo 2014-05-13tmh
if nargin>6
    if ~isempty(params_lfpdMC)
        %GET MONTECARLO PARAMS 2014-05-13tmh
        mcAlpha=params_lfpdMC.alph;
        mcMinLag=params_lfpdMC.minLag;

        toMC=true;
    else %if paramsMC is empty
        toMC=false;
    end
else
    toMC=false;
end


% PRE-PROCESS:
%==========================================================================

%% MAKE THE LIST OF UNITS IF IT IS NOT DEFINED
if isempty(unitList)
    unitList=makeUnitList(unit,maxUnitCh,maxUnitCode,badUnitCh,minSpRate);
end
if VERBOSITY_LEVEL>0
    fprintf('Unit List:\r')
    disp(unitList)
end

nUnits=size(unitList,1);

%% USE THE CORRECT LFPs:
if isempty(lfpChList)
    lfpChList1=lfp.chan;
    
    %remove bad LFPs
    badLFPs=badAlways(badAlways<=maxLFPCh);
    lfpChList1(:,badLFPs)=[];
    
    %if there are units of interest then leave out those LFP channels
    if ~isempty(unitsOfInt)
        logi=ismember(lfpChList1,unitsOfInt(:,1),'legacy'); %legacy behavior (new behavior from R2013a)
        lfp_onUnitCh=lfpChList1(logi);
        lfpChList1=lfpChList1(~logi);
    else
        lfp_onUnitCh=[];
    end
    
    %remove LFPs higher than maxLFPchan
    lfpChList=lfpChList1(lfpChList1<=maxLFPCh);
    
else
    badLFPs=[];
end

if VERBOSITY_LEVEL>1
    fprintf('%d LFPs used (ignoring %d LFPs) when building decoder: ',length(lfpChList),length(lfp.chan)-length(lfpChList));
    if exist('lfpChList1','var')
        fprintf('\r');
        fprintf('LFPs on unitOfInt channels: ');
        disp(lfp_onUnitCh)
        fprintf('\bBad LFPs: ');
        disp(badLFPs)
        fprintf('\bLFPs>maxLFPCh: ');
        disp(lfpChList1(lfpChList1>maxLFPCh))
    else
        fprintf('as defined by input lfpChList')
    end
    
    fprintf('\rlfpChList:')
    disp(lfpChList)    
end

lfpsF=lfp.lfpsF(:,lfpChList);
% lfpChansUsed=lfpChList;
lfpColors=lfpColors(lfpChList);

%% SET THE FITRANGE AND XVALRANGE
dlen=size(lfpsF,1);
nLFPs=size(lfpsF,2);

fitRange=  1:floor(fitProp*dlen);
xvalRange= ceil(fitProp*dlen):dlen;


%% GET THE FIRING RATE DATA
tellme(3,'Firing rates of units:');
%bins=0:0.001:0.1;
for uu=1:nUnits
    chan=unitList(uu,1);
    code=unitList(uu,2);
    meanSpRate(:,uu)=length(unit(chan,code).spTimes)/max(unit(chan,code).spTimes);
    spRlow(:,uu)=detrend(unit(chan,code).spRate,'constant'); %firing rates sampled at a 'low' Fs
    spRf(:,uu)=detrend(unit(chan,code).spRateF,'constant'); %those LP filtered
    
    if nargout>4
        %2014-06-16tmh : NatNeuro appeal; data for Andy
        extras.spRlow(:,uu)=unit(chan,code).spRate; %undetrended FR
    end
end

% BUILD DECODER
%==========================================================================
tellme(1,'\r\rTraining the LFP decoder from first %d%% of data...',fitProp*100)

%% linear model to generate 'spike-trig avgs'
H = filMIMO3(spRlow(fitRange,:),lfpsF(fitRange,:),width,2,1);
%Hpca=zeros(nUnits*(nWidth-1),nlfp);
feature=zeros(nUnits,width-1,nComp);

%pca on those 'STAs' to extract 'features'
if nargout>4
    tSTA  =((1:width-1)-width/2)/lfp.srLFPlow;
end
for uu=1:nUnits
    Hsub=H((uu-1)*(width-1)+1:uu*(width-1),:);
    if nargout>4
        staMIMO(:,:,uu)=Hsub;
    end

    [~,score]=princomp(Hsub);
    feature(uu,:,:)=score(:,1:nComp);
    
    if nargout>4
        firstNpcs(:,:,uu)=squeeze(feature(uu,:,1:nComp));
    end
end
clear Hsub

%% Reproject LFPs
lfpRP=nan(size(lfpsF,1),nUnits,nComp); %lfp reprojected
spRcomp=nan(length(fitRange),nUnits,nComp); %spike rate components
projMat=zeros(nUnits,nLFPs,nComp); %projection matrix

for uu=1:nUnits
    for c=1:nComp
        %convolve firing rate with PC features to generate signal for matching
        spRcomp(:,uu,c)=filter22(feature(uu,:,c),spRlow(fitRange,uu),2);
        %match actual LFPs to that using linear regression to generate projection matrix
        projMat(uu,:,c)=regress(spRcomp(:,uu,c),lfpsF(fitRange,:));
%below here for post-hoc        
        %reproject the lfp data in that space
        lfpRP(:,uu,c)=lfpsF*projMat(uu,:,c)';
    end
end

%add noise to the signal to stabilise the Wiener filter
ml=mean(mean(mean(abs(lfpRP))))/noiseF;
lfpRP=lfpRP+ml*randn(size(lfpRP,1),size(lfpRP,2),size(lfpRP,3));

tellme(1,' DONE')



%% MAKE THE PREDICTION FILTERS (Hpred) and test

Hpred=cell(1,nUnits); %filter container
spRpred=nan(size(spRf,1),nUnits); %predicted spike rate
% %containers for test results
testResB.r.train =nan(1,nUnits);
testResB.r.xval  =nan(1,nUnits);

tellme(1,'\rTesting the LFP decoder with last %d%% of data...',(1-fitProp)*100)
for uu=1:nUnits

%% generate the filters (Hpred) over the fitrange
    Hpred{uu}=filMIMO3(squeeze(lfpRP(fitRange,uu,:)),spRf(fitRange,uu),width,2,1);
    %apply filters to reprojected LFP data to predict actual FR
    spRpred(:,uu)=predMIMO3(squeeze(lfpRP(:,uu,:)),Hpred{uu},2,1);
    
    
    %compare prediction to actual FR over the xvalrange
    tellme(2,'\rTesting the LFP decoder with last %d%% of data, unit(%d), {%i,%i}...',...
        (1-fitProp)*100,uu,unitList(uu,1),unitList(uu,2))
    %for xval data
    %(at 5Hz)
    cc=corrcoef([spRpred(xvalRange,uu) spRf(xvalRange,uu)]);
    testResB.r.xval(uu)=cc(1,2);

%% RUN the bootstrap test to generate a synthetic distribution of r-values and test actual value.
    if toMC
        
        [testResB.r.xval_buildSig(uu),~,testResB.r.xval_buildSigThresh(uu)]=...
            lfpDecoder_montecarlo(  spRpred(xvalRange,uu),... %estimated firing rate
                                    spRf(xvalRange,uu),... %actual firing rate
                                    lfp.srLFPlow,... %Fs
                                    mcMinLag,... %minimum lag for the montecarlo
                                    mcAlpha,...  %sig level
                                    [],...  %don't plot XC
                                    []) ;  %don't histogram

    else
        %don't do MC : won't error, because output testResB is still extant.
    end
       
    %for training data
    %(at 5Hz)
    cc=corrcoef([spRpred(fitRange,uu) spRf(fitRange,uu)]);
    testResB.r.train(uu)=cc(1,2);
    
    tellme(2,'%.3f',testResB.r.xval(uu))    
end

%% FORMAT OUTPUT VARIABLES

decoderLFP.type='PCAofSRSP';
decoderLFP.buildparams=params_lfpdBu;
decoderLFP.unitList =unitList;
decoderLFP.lfpChList =lfpChList;

decoderLFP.buildparams.lfpColors=lfpColors;
decoderLFP.projMat=projMat;
decoderLFP.Hpred=cell2mat(Hpred); %(~2000 x nUnits)
decoderLFP.meanSpRate=meanSpRate;
decoderLFP.fitRange=[fitRange(1) fitRange(end)];
decoderLFP.xvalRange=[xvalRange(1) xvalRange(end)];

if nargout>4
    extras.staMIMO=staMIMO;
    extras.firstNpcs=firstNpcs;
    extras.tSTA=tSTA;
    extras.lfpRP=lfpRP;
    extras.spRf=spRf;
    extras.spRpred=spRpred;
end

tellme(1,'\rDONE\r')

end %of main function