function [H,r,thresh,c,C,hFig,cPop]=lfpDecoder_montecarlo(sigPred,sigActu,sRate,minLag,alph,toPlotXC,toHist)
% function [H,r,thresh,c,C,hFig,cPop]=lfpDecoder_montecarlo(sigPred,sigActu,sRate,minLag,alph,toPlotXC,toHist)

%transpose to row vectors if necessary
if size(sigPred,1)>size(sigPred,2)
    sigPred=sigPred';
    sigActu=sigActu';
end

%remove mean then unity STD
a=(sigActu-mean(sigActu))./std(sigActu,1);  %andy helped with this normalisation
                                            %note not the default matlab std
p=(sigPred-mean(sigPred))./std(sigPred,1);

%make the length an even number
if mod(length(a),2)
    a(end)=[];
    p(end)=[];
end

%pre-pad actual with zeros at both ends
x=[zeros(1,length(a)/2),a,zeros(1,length(a)/2)];
%pad each with half at beginning and end ([2/2,1/2,2/2,1/2])
y=[p((length(p)/2+1):length(p)),p,p(1:length(p)/2)];

[C,lags]=xcorr(x,y,length(a)/2);
C=C./length(a); %normalise

c=C((length(C)+1)/2); %should be equal to the corrcoef
ccap=corrcoef(a,p);
r=ccap(1,2);

minLagS=minLag*sRate; %remove the central values from the xcorr
cPop=C(~(lags>=-minLagS & lags<=minLagS));

%alph is the significance level
prop=1-(alph/2); %two-tailed test, e.g. alpha=0.05 --> test above 97.5% of data
iThresh=ceil(length(cPop)*prop);
if iThresh>=length(cPop)
    warning('not enough data for this significance level: threshold == maximum value in distribution.')
end
cList=sort(cPop);
thresh=cList(iThresh);
H=r>=thresh;

%this was the code which did it as a proportion of the standard deviation,
%which was silly:
% thresh=sigmult*std(cPop,1);
% H=r>thresh;

if ~isempty(toPlotXC)
    if toPlotXC==0
        hFig(1)=figure;
    else
        hFig(1)=toPlotXC;
    end
    figure(hFig(1))
    clf
    plot(lags/sRate,C)
    xlabel('lags (sec)')
    ylabel('xcorr')
    
    hold on
    plot([-minLag -minLag],[0 0.5],'r:')
    plot([minLag minLag],[0 0.5],'r:')
end

if ~isempty(toHist)
    if toHist==0
        hFig(2)=figure;
    else
        hFig(2)=toHist;
    end
    figure(hFig(2))
    clf
    hist(cPop,40)
    hold on
    plot(thresh,0,'g*')
    plot(r,0,'r+')
    title(sprintf('alpha=%f, thresh=%d(*), r=%d(+)',alph,thresh,r))
    xlabel('r')
    ylabel('count')
end

end