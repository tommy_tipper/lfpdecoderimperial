function unitList=makeUnitList(units,maxUnitCh,maxUnitCode,badUnitCh,minSpRate)

% 2014-05-06tmh : added 'legacy' to ismember behavior.

unitList=[];
uu=0;
for uCh=1:size(units,1)
    for uCo=1:size(units,2)
        if ~isempty(units(uCh,uCo).spTimes)
            unitList=[unitList;[uCh,uCo]]; %#ok<AGROW>
            uu=uu+1;
            meanSpRate(uu,1)=length(units(uCh,uCo).spTimes)/max(units(uCh,uCo).spTimes); %#ok<AGROW>
        end
    end
end

%only output those channels:
% <=maxUnitCh
logiCh=unitList(:,1)<=maxUnitCh;
% AND <=maxUnitCode
logiCode=unitList(:,2)<=maxUnitCode;
%AND NOT a bad unit channel
if ~isempty(badUnitCh)
    logiBadCh=ismember(unitList(:,1),badUnitCh,'legacy'); %2014-05-06tmh 'legacy'
else
    logiBadCh=unitList(:,1)==inf; %i.e. no channels are bad
end
%AND greater than a certain spike rate
logiMinSpRate=meanSpRate>minSpRate;

unitList=unitList(logiCh & logiCode & ~logiBadCh & logiMinSpRate, :);

end